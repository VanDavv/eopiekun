package eopiekun.tables;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import eopiekun.controllers.MainController;
import eopiekun.models.Child;
import eopiekun.models.Event;
import eopiekun.models.SignedChild;
import eopiekun.web.ChildManager;
import eopiekun.web.EventManager;
import eopiekun.web.SearchTypes;
import eopiekun.web.SignedChildManager;

public class Adder {
	private static ObservableList<ChildTableModel> childs;
	private static ObservableList<EventTableModel> events;
	private static ObservableList<SignedChildTableModel> signedChilds;

	public static void refresh() {
		List<Child> childList = new ChildManager().search(SearchTypes.all, "");
		List<ChildTableModel> convertedChilds = new ArrayList<ChildTableModel>();
		for(Child c : childList) {
			convertedChilds.add(ChildTableModel.convert(c));
		}
		childs = FXCollections.observableArrayList(convertedChilds);
		
		List<Event> eventList = new EventManager().search(SearchTypes.all, "");
		List<EventTableModel> convertedEvents = new ArrayList<EventTableModel>();
		for(Event e : eventList) {
			convertedEvents.add(EventTableModel.convert(e));
		}
		events = FXCollections.observableArrayList(convertedEvents);
		
		List<SignedChild> signedChildList = new SignedChildManager().search(SearchTypes.all, "");
		List<SignedChildTableModel> convertedSignedChilds = new ArrayList<SignedChildTableModel>();
		for(SignedChild sc : signedChildList) {
			convertedSignedChilds.add(SignedChildTableModel.convert(sc));
		}
		signedChilds = FXCollections.observableArrayList(convertedSignedChilds);
	}
	
	public Adder(MainController main) {
		refresh();
		//----------------------CHILDS------------------------------------
			TableColumn<ChildTableModel, String> addSearchedFirstNameColumn = main.getAddSearchedFirstNameColumn();
			TableColumn<ChildTableModel, String> addSearchedLastNameColumn = main.getAddSearchedLastNameColumn();
			TableColumn<ChildTableModel, String> addSearchedClassIdColumn = main.getAddSearchedClassIdColumn();

			addSearchedFirstNameColumn.setCellValueFactory(cellData -> cellData.getValue().getFirstName());
			addSearchedLastNameColumn.setCellValueFactory(cellData -> cellData.getValue().getLastName());
			addSearchedClassIdColumn.setCellValueFactory(cellData -> cellData.getValue().getClassId());

			main.setAddSearchedFirstNameColumn(addSearchedFirstNameColumn);
			main.setAddSearchedLastNameColumn(addSearchedLastNameColumn);
			main.setAddSearchedClassIdColumn(addSearchedClassIdColumn);
			
			TableView<ChildTableModel> childTableView = main.getChildTableView();
			childTableView.setItems(childs);
			main.setChildTableView(childTableView);
			
		//----------------------EVENTS------------------------------------
			TableColumn<EventTableModel, String> eventColumn = main.getEventColumn();
			
			eventColumn.setCellValueFactory(cellData -> cellData.getValue().getEventTitle());
			
			main.setEventColumn(eventColumn);
			
			TableView<EventTableModel> eventTableViev = main.getEventTableViev();
			eventTableViev.setItems(events);
			main.setEventTableViev(eventTableViev);
			
		//----------------------SIGNEDCHILDS------------------------------------
			TableColumn<SignedChildTableModel, String> firstNameColumn = main.getFirstNameColumn();
			TableColumn<SignedChildTableModel, String> lastNameColumn = main.getLastNameColumn();
			TableColumn<SignedChildTableModel, String> classIdColumn = main.getClassIdColumn();
			TableColumn<SignedChildTableModel, String> addingTimeColumn = main.getAddingTimeColumn();
			TableColumn<SignedChildTableModel, String> statusColumn = main.getStatusColumn();

			firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().getFirstName());
			lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().getLastName());
			classIdColumn.setCellValueFactory(cellData -> cellData.getValue().getClassId());
			addingTimeColumn.setCellValueFactory(cellData -> cellData.getValue().getAddingTime());
			statusColumn.setCellValueFactory(cellData -> cellData.getValue().getStatus());
			
			main.setFirstNameColumn(firstNameColumn);
			main.setLastNameColumn(lastNameColumn);
			main.setClassIdColumn(classIdColumn);
			main.setAddingTimeColumn(addingTimeColumn);
			main.setStatusColumn(statusColumn);
			
			TableView<SignedChildTableModel> signedChildTableView = main.getSignedChildTableView();
			signedChildTableView.setItems(signedChilds);
			main.setSignedChildTableView(signedChildTableView);
			
	}

	public static void addChild(Child c) {
		childs.add(ChildTableModel.convert(c));
	}
	
	public static void addEvent(Event e) {
		events.add(EventTableModel.convert(e));
	}
	
	public static void addSignedChild(SignedChild sc) {
		signedChilds.add(SignedChildTableModel.convert(sc));
	}
}
