package eopiekun.tables;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import eopiekun.models.Event;



public class EventTableModel {
	
	private StringProperty eventTitle;
	private StringProperty eventDesc;
	private StringProperty eventAuth;
	private IntegerProperty eventPriority;
	
	public static List<EventTableModel> convert(List<Event> arg) {
		List<EventTableModel> result = new ArrayList<EventTableModel>();
		
		for(Event e : arg) {
			result.add(convert(e));
		}
		
		return result;
	}
	
	public static EventTableModel convert(Event e) {
		EventTableModel newEvent = new EventTableModel();
		newEvent.setEventTitle(new SimpleStringProperty(e.getEventTitle()));
		newEvent.setEventDesc(new SimpleStringProperty(e.getEventDesc()));
		newEvent.setEventAuth(new SimpleStringProperty(e.getEventAuth()));
		newEvent.setEventPriority(new SimpleIntegerProperty(e.getEventPriority()));
		return newEvent;
	}

	public StringProperty getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(StringProperty eventTitle) {
		this.eventTitle = eventTitle;
	}

	public StringProperty getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(StringProperty eventDesc) {
		this.eventDesc = eventDesc;
	}

	public StringProperty getEventAuth() {
		return eventAuth;
	}

	public void setEventAuth(StringProperty eventAuth) {
		this.eventAuth = eventAuth;
	}

	public IntegerProperty getEventPriority() {
		return eventPriority;
	}

	public void setEventPriority(IntegerProperty eventPriority) {
		this.eventPriority = eventPriority;
	}

}
