package eopiekun.tables;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import eopiekun.models.SignedChild;

public class SignedChildTableModel{
	private StringProperty firstName;
	private StringProperty lastName;
	private StringProperty classId;
	private StringProperty addingTime;
	private StringProperty status;
	
	public static List<SignedChildTableModel> convert(List<SignedChild> arg) {
		List<SignedChildTableModel> result = new ArrayList<SignedChildTableModel>();
		
		for(SignedChild sc : arg) {
			result.add(convert(sc));
			
		}
		
		return result;
	}
	
	public static SignedChildTableModel convert(SignedChild sc) {
		
		SignedChildTableModel newSignedChild = new SignedChildTableModel();
		newSignedChild.setLastName(new SimpleStringProperty(sc.getLastName()));
		newSignedChild.setFirstName(new SimpleStringProperty(sc.getFirstName()));
		newSignedChild.setClassId(new SimpleStringProperty(sc.getClassId()));
		newSignedChild.setAddingTime(new SimpleStringProperty(sc.getAddingTime().toString()));
		newSignedChild.setStatus(new SimpleStringProperty(sc.getStatus()));
		
		return newSignedChild;
	}
	
	public StringProperty getFirstName() {
		return firstName;
	}
	public void setFirstName(StringProperty firstName) {
		this.firstName = firstName;
	}
	public StringProperty getLastName() {
		return lastName;
	}
	public void setLastName(StringProperty lastName) {
		this.lastName = lastName;
	}
	public StringProperty getClassId() {
		return classId;
	}
	public void setClassId(StringProperty classId) {
		this.classId = classId;
	}
	public StringProperty getAddingTime() {
		return addingTime;
	}
	public void setAddingTime(StringProperty addingTime) {
		this.addingTime = addingTime;
	}
	public StringProperty getStatus() {
		return status;
	}
	public void setStatus(StringProperty status) {
		this.status = status;
	}
	
}
