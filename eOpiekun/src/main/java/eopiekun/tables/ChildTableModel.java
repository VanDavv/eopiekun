package eopiekun.tables;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import eopiekun.models.Child;

public class ChildTableModel {
	private StringProperty firstName;
	private StringProperty lastName;
	private StringProperty classId;
	
	public static List<ChildTableModel> convert(List<Child> arg) {
		List<ChildTableModel> result = new ArrayList<ChildTableModel>();
		
		for(Child c : arg) {
			result.add(convert(c));
		}
		
		return result;
	}
	
	public static ChildTableModel convert(Child c) {
		ChildTableModel newChild = new ChildTableModel();
		newChild.setFirstName(new SimpleStringProperty(c.getFirstName()));
		newChild.setLastName(new SimpleStringProperty(c.getLastName()));
		newChild.setClassId(new SimpleStringProperty(c.getClassId()));
		return newChild;
	}
	
	public StringProperty getFirstName() {
		return firstName;
	}
	public void setFirstName(StringProperty firstName) {
		this.firstName = firstName;
	}
	public StringProperty getLastName() {
		return lastName;
	}
	public void setLastName(StringProperty lastName) {
		this.lastName = lastName;
	}
	public StringProperty getClassId() {
		return classId;
	}
	public void setClassId(StringProperty classId) {
		this.classId = classId;
	}
	
}
