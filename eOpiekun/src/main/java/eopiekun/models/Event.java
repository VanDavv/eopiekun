package eopiekun.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Event {
	@Id
	@GeneratedValue
	private int id;
	private String eventTitle;
	private String eventDesc;
	private String eventAuth;
	private int eventPriority;
	
	public String getEventAuth() {
		return eventAuth;
	}

	public void setEventAuth(String eventAuth) {
		this.eventAuth = eventAuth;
	}

	public int getEventPriority() {
		return eventPriority;
	}

	public void setEventPriority(int eventPriority) {
		this.eventPriority = eventPriority;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}
}
