package eopiekun.tools.main;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javafx.scene.control.TextField;
import eopiekun.controllers.MainController;
import eopiekun.tools.PropManager;

public class Validator {
	Map<String, TextField> fields = new HashMap<String, TextField>();
	Map<String, String> values = new HashMap<String, String>();

	public Validator(final MainController main) {
				fields.put("imie", main.getAddFirstNameField());
				fields.put("nazwisko", main.getAddLastNameField());
				fields.put("klasa", main.getAddClassIdField());

				values.put("imie", fields.get("imie").getText());
				values.put("nazwisko", fields.get("nazwisko").getText());
				values.put("klasa", fields.get("klasa").getText());

	}

	public Properties getValidationRules() {
		return new PropManager().getProperties();
	}

	public boolean validate() {
		for (String key : values.keySet()) {
			if (values.get(key) == null) {
				setUnvalidated(fields.get(key));
				return false;
			}
		}

		Map<?, ?> rules = getValidationRules();

		for (String key : values.keySet()) {
			for (Object s : rules.keySet()) {
				String regKey = s + "";
				String regex = rules.get(s) + "";
				if (key.equals(regKey))
					if (!(values.get(key).matches(regex))) {
						setUnvalidated(fields.get(key));
						return false;
					} else
						break;
			}
		}

		return true;
	}

	private TextField Unvalidated;

	public TextField getUnvalidated() {
		return Unvalidated;
	}

	private void setUnvalidated(TextField unvalidated) {
		Unvalidated = unvalidated;
	}
}