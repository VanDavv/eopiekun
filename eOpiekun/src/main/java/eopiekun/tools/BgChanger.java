package eopiekun.tools;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Paint;

public class BgChanger {
	private static Map<TextField, Background> changed = new HashMap<TextField,Background>();
	
	public static void change(TextField field, String color) {
		if(!(changed.containsKey(field)))
			changed.put(field, field.getBackground());
		field.setBackground(new Background(
					new BackgroundFill(Paint.valueOf(color),null,null)
					));
	}
	
	public static void undoAll() {
		for(TextField field : changed.keySet()) {
			field.setBackground(changed.get(field));
		}
	}
}
