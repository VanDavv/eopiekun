package eopiekun.tools;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropManager {
	private Properties props = null;
	private final String path = "src/main/resources/validation.properties";
	public PropManager() {
		load();
	}

	private void load() {
		try {
			FileInputStream in = new FileInputStream(path);
			props = new Properties();
			props.load(in);
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void save() {
		try {
			FileOutputStream out = new FileOutputStream(path);
			props.store(out, null);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Properties getProperties() {
		return props;
	}

	public void setProperties(Properties props) {
		this.props = props;
	}
}
