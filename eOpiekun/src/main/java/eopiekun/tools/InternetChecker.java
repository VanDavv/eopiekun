package eopiekun.tools;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

public class InternetChecker {	
	public static boolean isInternetReachable()
    {
        try {
            URL url = new URL("http://www.google.com");
            
            HttpURLConnection urlConnect = (HttpURLConnection)url.openConnection();
            urlConnect.setConnectTimeout(200);
            Object objData = urlConnect.getContent();
            
            System.out.println(objData);
            
        	} 
			        catch (UnknownHostException e) {
			        	return false;
			        }
        
			        catch (IOException e) {
			        	return false;
			        }
        return true;
    }
}
