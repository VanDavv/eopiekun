package eopiekun.tools;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import eopiekun.models.Event;
import eopiekun.tables.Adder;

public class AppTimer {
	Timer timer = new Timer((1000 * 60),new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
		 refresh();
		}
		});

	public void start() {timer.start();}
	public void stop() {timer.stop();}
	
	private void refresh() {
		if (!InternetChecker.isInternetReachable()) {
			Event event = new Event();
			event.setEventAuth("Application");
			event.setEventDesc("Sprawd� po��czenie internetowe. Je�eli internet dzia�a a komunikat wci�� wyskakuje, skontaktuj si� z administratorem");
			event.setEventPriority(2);
			event.setEventTitle("Brak internetu!");
			Adder.addEvent(event);
		}
		Adder.refresh();
	}
}
