package eopiekun.web;

import java.util.List;

import javax.persistence.TypedQuery;

import eopiekun.models.SignedChild;

public class SignedChildManager extends Manager{
	
	public void persist(SignedChild signedChild) {
		manager.getTransaction().begin();
		manager.persist(signedChild);
		manager.getTransaction().commit();
	}
	
	public void remove(SignedChild signedChild) {
		manager.getTransaction().begin();
		manager.remove(signedChild);
		manager.getTransaction().commit();		
	}
	
	public void refresh(SignedChild signedChild) {
		manager.getTransaction().begin();
		manager.refresh(signedChild);
		manager.getTransaction().commit();
	}
	
	public List<SignedChild> search(SearchTypes type, String value) {
		String queryValue = "";
		switch(type) {
			case all: {
				queryValue = "select sc from SignedChild sc";
				break;
			}
			case classId:{
				queryValue = "select sc from SignedChild sc where sc.classId = :value";
				break;
			}
			case firstName: {
				queryValue = "select sc from SignedChild sc where sc.firstName = :value";
				break;
			}
			case lastName: {
				queryValue = "select sc from SignedChild sc where sc.lastName = :value";
				break;
			}
			case addingTime: {
				queryValue = "select sc from SignedChild sc where sc.addingTime = :value";
				break;
			}
			case status: {
				queryValue = "select sc from SignedChild sc where sc.status = :value";
				break;
			}
			default:
				return null;
		}
		
		TypedQuery<SignedChild> query = manager.createQuery(queryValue, SignedChild.class);
		if (!(type == SearchTypes.all)) query.setParameter("value", value);
		
		return query.getResultList();
	}
}

