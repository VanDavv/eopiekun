package eopiekun.web;

import java.util.List;

import javax.persistence.TypedQuery;

import eopiekun.models.Child;
import eopiekun.models.Event;

public class EventManager extends Manager{
	
	public void persist(Event event) {
		manager.getTransaction().begin();
		manager.persist(event);
		manager.getTransaction().commit();
	}
	
	public void remove(Event event) {
		manager.getTransaction().begin();
		manager.remove(event);
		manager.getTransaction().commit();		
	}
	
	public void refresh(Child event) {
		manager.getTransaction().begin();
		manager.refresh(event);
		manager.getTransaction().commit();
	}
	
	public List<Event> search(SearchTypes type, String value) {
		String queryValue = "";
		switch(type) {
			case all: {
				queryValue = "select e from Event e";
				break;
			}
			case eventAuth:{
				queryValue = "select e from Event e where e.eventAuth = :value";
				break;
			}
			case eventDesc: {
				queryValue = "select e from Event e where e.eventDesc = :value";
				break;
			}
			case eventPriority: {
				queryValue = "select e from Event e where e.eventPriority = :value";
				break;
			}
			case eventTitle: {
				queryValue = "select e from Event e where e.eventTitle = :value";
				break;
			}
			default:
				return null;
		}
		
		TypedQuery<Event> query = manager.createQuery(queryValue, Event.class);
		if (!(type == SearchTypes.all)) query.setParameter("value", value);
		
		return query.getResultList();
	}
}
