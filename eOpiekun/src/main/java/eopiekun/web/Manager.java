package eopiekun.web;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class Manager {
	protected static EntityManagerFactory factory = Persistence.createEntityManagerFactory("eDatabase");
	protected static EntityManager manager = factory.createEntityManager();
	
	public static void close() {
		manager.close();
		factory.close();
	}
	public static EntityManagerFactory getFactory() {
		return factory;
	}
	public static void setFactory(EntityManagerFactory factory) {
		Manager.factory = factory;
	}
	public static EntityManager getManager() {
		return manager;
	}
	public static void setManager(EntityManager manager) {
		Manager.manager = manager;
	}
	
	
}
