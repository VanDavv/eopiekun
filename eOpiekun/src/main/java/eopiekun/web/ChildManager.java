package eopiekun.web;

import java.util.List;

import javax.persistence.TypedQuery;

import eopiekun.models.Child;

public class ChildManager extends Manager{
	public void persist(Child child) {
		manager.getTransaction().begin();
		manager.persist(child);
		manager.getTransaction().commit();
	}
	
	public void remove(Child child) {
		manager.getTransaction().begin();
		manager.remove(child);
		manager.getTransaction().commit();		
	}
	
	public void refresh(Child child) {
		manager.getTransaction().begin();
		manager.refresh(child);
		manager.getTransaction().commit();
	}
	
	public List<Child> search(SearchTypes type, String value) {
		String queryValue = "";
		switch(type) {
			case all: {
				queryValue = "select c from Child c";
				break;
			}
			
			case classId:{
				queryValue = "select c from Child c where c.classId = :value";
				break;
			}
			case firstName: {
				queryValue = "select c from Child c where c.firstName = :value";
				break;
			}
			case lastName: {
				queryValue = "select c from Child c where c.lastName = :value";
				break;
			}
			default:
				return null;
		}
		
		TypedQuery<Child> query = manager.createQuery(queryValue, Child.class);
		if (!(type == SearchTypes.all)) query.setParameter("value", value);
		
		return query.getResultList();
	}
}
