package eopiekun.controllers;

import javafx.scene.Node;
import eopiekun.models.SignedChild;
import eopiekun.tables.Adder;
import eopiekun.tools.BgChanger;
import eopiekun.tools.main.Validator;
import eopiekun.web.SignedChildManager;

public class Actions {
	public static void closeAction(Node node) {
		node.getScene().getWindow().hide();
	}
	//test commit
	public static void addAddChildAction(MainController mainController) {
		BgChanger.undoAll();

		if (!mainController.getAddForceAddBox().isSelected()) {
			Validator validator = new Validator(mainController);

			if (!validator.validate()) {
				ErrorController.newError("Pole "
						+ validator.getUnvalidated().getId()
						+ " jest b��dnie uzupe�nione. Popraw b��dy!");
				BgChanger.change(validator.getUnvalidated(), "red");
				return;
			}
		}
		
		SignedChild signedChild = new SignedChild();
		signedChild.setFirstName(mainController.getAddFirstNameField().getText());
		signedChild.setLastName(mainController.getAddLastNameField().getText());
		signedChild.setClassId(mainController.getAddClassIdField().getText());
		signedChild.setStatus("Dodany");
		
		new SignedChildManager().persist(signedChild);
		new SignedChildManager().refresh(signedChild);

		Adder.addSignedChild(signedChild);
		
		addClearAction(mainController);
	}
	
	public static void addClearAction(MainController mainController) {
		BgChanger.undoAll();
		mainController.getAddFirstNameField().clear();
		mainController.getAddLastNameField().clear();
		mainController.getAddClassIdField().clear();
	}

	public static void aboutAction() {
		ErrorController.newError("Application made by �ukasz Pi�atowski.\r\n All rights reserved!", "About");
	}
}
