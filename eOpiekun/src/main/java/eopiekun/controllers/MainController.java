package eopiekun.controllers;

import java.io.IOException;

import eopiekun.tables.ChildTableModel;
import eopiekun.tables.EventTableModel;
import eopiekun.tables.SignedChildTableModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class MainController extends VBox {
	public MainController() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("frames/MainFrame.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			ErrorController.newError("FXMLLoader.load() exception in MainController.java \r\n" + exception.getMessage());
		}
	}
	
	@FXML
    private MenuItem clearMenuItem;
    @FXML
    private Button deleteButton;
    @FXML
    private TextField birthDateField;
    @FXML
    private MenuItem aboutMenuItem;
    @FXML
    private TextField contactOneField;
    @FXML
    private TextField addFirstNameField;
    @FXML
    private ImageView imageView;
    @FXML
    private AnchorPane eventDescPane;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField firstNameField;
    @FXML
    private MenuBar menuBar;
    @FXML
    private Button premissionsButton;
    @FXML
    private Button clearButton;
    @FXML
    private TableColumn<ChildTableModel, String> addSearchedFirstNameColumn;
    @FXML
    private AnchorPane quickAddPane;
    @FXML
    private TableView<ChildTableModel> childTableView;
    @FXML
    private TableView<SignedChildTableModel> signedChildTableView;
    @FXML
    private Button saveButton;
    @FXML
    private TableColumn<EventTableModel, String> eventColumn;
    @FXML
    private Button dismissButton;
    @FXML
    private AnchorPane middlePane;
    @FXML
    private Menu helpMenu;
    @FXML
    private Button detailsButton;
    @FXML
    private Menu applicationMenu;
    @FXML
    private TextField groupIdField;
    @FXML
    private Button addClearButton;
    @FXML
    private Button scheduleButton;
    @FXML
    private TableView<EventTableModel> eventTableViev;
    @FXML
    private TextArea eventDescArea;
    @FXML
    private AnchorPane eventTablePane;
    @FXML
    private TableColumn<ChildTableModel, String> addSearchedLastNameColumn;
    @FXML
    private CheckBox addForceAddBox;
    @FXML
    private Menu editMenu;
    @FXML
    private TextField contactTwoField;
    @FXML
    private VBox mainVBox;
    @FXML
    private Button addAddButton;
    @FXML
    private TextField addLastNameField;
    @FXML
    private TextField addClassIdField;
    @FXML
    private Button editButton;
    @FXML
    private MenuItem closeMenuItem;
    @FXML
    private TextField classIdField;
    @FXML
    private TableColumn<ChildTableModel, String> addSearchedClassIdColumn;
    @FXML
    private SplitPane popUpPane;
    @FXML
    private TextField addressField;
    @FXML
    private TableColumn<SignedChildTableModel, String> firstNameColumn;
    @FXML
    private TableColumn<SignedChildTableModel, String> statusColumn;
    @FXML
    private TableColumn<SignedChildTableModel, String> classIdColumn;
    @FXML
    private TableColumn<SignedChildTableModel, String> addingTimeColumn;  
    @FXML
    private TableColumn<SignedChildTableModel, String> lastNameColumn;

    @FXML
    void closeMenuItemAction(ActionEvent event) {
    	Actions.closeAction(this);
    }

    @FXML
    void clearMenuItemAction(ActionEvent event) {
    	Actions.addClearAction(this);
    	//Actions.ClearAction(this);
    }

    @FXML
    void aboutMenuItemAction(ActionEvent event) {
    	Actions.aboutAction();
    }

    @FXML
    void addAddButtonAction(ActionEvent event) {
    	Actions.addAddChildAction(this);
    }

    @FXML
    void addAddClearButton(ActionEvent event) {
    	Actions.addClearAction(this);
    }

    @FXML
    void scheduleAction(ActionEvent event) {

    }

    @FXML
    void editAction(ActionEvent event) {

    }

    @FXML
    void clearAction(ActionEvent event) {

    }

    @FXML
    void saveAction(ActionEvent event) {

    }

    @FXML
    void deleteAction(ActionEvent event) {

    }

    @FXML
    void premissionsAction(ActionEvent event) {

    }

    @FXML
    void detailsAction(ActionEvent event) {

    }

    @FXML
    void dismissAction(ActionEvent event) {

    }
    
//OneClickAdd-----------------------------------------------------------------------------------------------------------------------
    public MenuItem getClearMenuItem() {
		return clearMenuItem;
	}

	public void setClearMenuItem(MenuItem clearMenuItem) {
		this.clearMenuItem = clearMenuItem;
	}

	public Button getDeleteButton() {
		return deleteButton;
	}

	public void setDeleteButton(Button deleteButton) {
		this.deleteButton = deleteButton;
	}

	public TextField getBirthDateField() {
		return birthDateField;
	}

	public void setBirthDateField(TextField birthDateField) {
		this.birthDateField = birthDateField;
	}

	public MenuItem getAboutMenuItem() {
		return aboutMenuItem;
	}

	public void setAboutMenuItem(MenuItem aboutMenuItem) {
		this.aboutMenuItem = aboutMenuItem;
	}

	public TextField getContactOneField() {
		return contactOneField;
	}

	public void setContactOneField(TextField contactOneField) {
		this.contactOneField = contactOneField;
	}

	public TextField getAddFirstNameField() {
		return addFirstNameField;
	}

	public void setAddFirstNameField(TextField addFirstNameField) {
		this.addFirstNameField = addFirstNameField;
	}

	public ImageView getImageView() {
		return imageView;
	}

	public void setImageView(ImageView imageView) {
		this.imageView = imageView;
	}

	public AnchorPane getEventDescPane() {
		return eventDescPane;
	}

	public void setEventDescPane(AnchorPane eventDescPane) {
		this.eventDescPane = eventDescPane;
	}

	public TextField getLastNameField() {
		return lastNameField;
	}

	public void setLastNameField(TextField lastNameField) {
		this.lastNameField = lastNameField;
	}

	public TextField getFirstNameField() {
		return firstNameField;
	}

	public void setFirstNameField(TextField firstNameField) {
		this.firstNameField = firstNameField;
	}

	public MenuBar getMenuBar() {
		return menuBar;
	}

	public void setMenuBar(MenuBar menuBar) {
		this.menuBar = menuBar;
	}

	public Button getPremissionsButton() {
		return premissionsButton;
	}

	public void setPremissionsButton(Button premissionsButton) {
		this.premissionsButton = premissionsButton;
	}

	public Button getClearButton() {
		return clearButton;
	}

	public void setClearButton(Button clearButton) {
		this.clearButton = clearButton;
	}

	public TableColumn<ChildTableModel, String> getAddSearchedFirstNameColumn() {
		return addSearchedFirstNameColumn;
	}

	public void setAddSearchedFirstNameColumn(
			TableColumn<ChildTableModel, String> addSearchedFirstNameColumn) {
		this.addSearchedFirstNameColumn = addSearchedFirstNameColumn;
	}

	public AnchorPane getQuickAddPane() {
		return quickAddPane;
	}

	public void setQuickAddPane(AnchorPane quickAddPane) {
		this.quickAddPane = quickAddPane;
	}

	public TableView<ChildTableModel> getChildTableView() {
		return childTableView;
	}

	public void setChildTableView(TableView<ChildTableModel> childTableView) {
		this.childTableView = childTableView;
	}

	public TableView<SignedChildTableModel> getSignedChildTableView() {
		return signedChildTableView;
	}

	public void setSignedChildTableView(
			TableView<SignedChildTableModel> signedChildTableView) {
		this.signedChildTableView = signedChildTableView;
	}

	public Button getSaveButton() {
		return saveButton;
	}

	public void setSaveButton(Button saveButton) {
		this.saveButton = saveButton;
	}

	public TableColumn<EventTableModel, String> getEventColumn() {
		return eventColumn;
	}

	public void setEventColumn(TableColumn<EventTableModel, String> eventColumn) {
		this.eventColumn = eventColumn;
	}

	public Button getDismissButton() {
		return dismissButton;
	}

	public void setDismissButton(Button dismissButton) {
		this.dismissButton = dismissButton;
	}

	public AnchorPane getMiddlePane() {
		return middlePane;
	}

	public void setMiddlePane(AnchorPane middlePane) {
		this.middlePane = middlePane;
	}

	public Menu getHelpMenu() {
		return helpMenu;
	}

	public void setHelpMenu(Menu helpMenu) {
		this.helpMenu = helpMenu;
	}

	public Button getDetailsButton() {
		return detailsButton;
	}

	public void setDetailsButton(Button detailsButton) {
		this.detailsButton = detailsButton;
	}

	public Menu getApplicationMenu() {
		return applicationMenu;
	}

	public void setApplicationMenu(Menu applicationMenu) {
		this.applicationMenu = applicationMenu;
	}

	public TextField getGroupIdField() {
		return groupIdField;
	}

	public void setGroupIdField(TextField groupIdField) {
		this.groupIdField = groupIdField;
	}

	public Button getAddClearButton() {
		return addClearButton;
	}

	public void setAddClearButton(Button addClearButton) {
		this.addClearButton = addClearButton;
	}

	public Button getScheduleButton() {
		return scheduleButton;
	}

	public void setScheduleButton(Button scheduleButton) {
		this.scheduleButton = scheduleButton;
	}

	public TableView<EventTableModel> getEventTableViev() {
		return eventTableViev;
	}

	public void setEventTableViev(TableView<EventTableModel> eventTableViev) {
		this.eventTableViev = eventTableViev;
	}

	public TextArea getEventDescArea() {
		return eventDescArea;
	}

	public void setEventDescArea(TextArea eventDescArea) {
		this.eventDescArea = eventDescArea;
	}

	public AnchorPane getEventTablePane() {
		return eventTablePane;
	}

	public void setEventTablePane(AnchorPane eventTablePane) {
		this.eventTablePane = eventTablePane;
	}

	public TableColumn<ChildTableModel, String> getAddSearchedLastNameColumn() {
		return addSearchedLastNameColumn;
	}

	public void setAddSearchedLastNameColumn(
			TableColumn<ChildTableModel, String> addSearchedLastNameColumn) {
		this.addSearchedLastNameColumn = addSearchedLastNameColumn;
	}

	public CheckBox getAddForceAddBox() {
		return addForceAddBox;
	}

	public void setAddForceAddBox(CheckBox addForceAddBox) {
		this.addForceAddBox = addForceAddBox;
	}

	public Menu getEditMenu() {
		return editMenu;
	}

	public void setEditMenu(Menu editMenu) {
		this.editMenu = editMenu;
	}

	public TextField getContactTwoField() {
		return contactTwoField;
	}

	public void setContactTwoField(TextField contactTwoField) {
		this.contactTwoField = contactTwoField;
	}

	public VBox getMainVBox() {
		return mainVBox;
	}

	public void setMainVBox(VBox mainVBox) {
		this.mainVBox = mainVBox;
	}

	public Button getAddAddButton() {
		return addAddButton;
	}

	public void setAddAddButton(Button addAddButton) {
		this.addAddButton = addAddButton;
	}

	public TextField getAddLastNameField() {
		return addLastNameField;
	}

	public void setAddLastNameField(TextField addLastNameField) {
		this.addLastNameField = addLastNameField;
	}

	public TextField getAddClassIdField() {
		return addClassIdField;
	}

	public void setAddClassIdField(TextField addClassIdField) {
		this.addClassIdField = addClassIdField;
	}

	public Button getEditButton() {
		return editButton;
	}

	public void setEditButton(Button editButton) {
		this.editButton = editButton;
	}

	public MenuItem getCloseMenuItem() {
		return closeMenuItem;
	}

	public void setCloseMenuItem(MenuItem closeMenuItem) {
		this.closeMenuItem = closeMenuItem;
	}

	public TextField getClassIdField() {
		return classIdField;
	}

	public void setClassIdField(TextField classIdField) {
		this.classIdField = classIdField;
	}

	public TableColumn<ChildTableModel, String> getAddSearchedClassIdColumn() {
		return addSearchedClassIdColumn;
	}

	public void setAddSearchedClassIdColumn(
			TableColumn<ChildTableModel, String> addSearchedClassIdColumn) {
		this.addSearchedClassIdColumn = addSearchedClassIdColumn;
	}

	public SplitPane getPopUpPane() {
		return popUpPane;
	}

	public void setPopUpPane(SplitPane popUpPane) {
		this.popUpPane = popUpPane;
	}

	public TextField getAddressField() {
		return addressField;
	}

	public void setAddressField(TextField addressField) {
		this.addressField = addressField;
	}

	public TableColumn<SignedChildTableModel, String> getFirstNameColumn() {
		return firstNameColumn;
	}

	public void setFirstNameColumn(
			TableColumn<SignedChildTableModel, String> firstNameColumn) {
		this.firstNameColumn = firstNameColumn;
	}

	public TableColumn<SignedChildTableModel, String> getStatusColumn() {
		return statusColumn;
	}

	public void setStatusColumn(
			TableColumn<SignedChildTableModel, String> statusColumn) {
		this.statusColumn = statusColumn;
	}

	public TableColumn<SignedChildTableModel, String> getClassIdColumn() {
		return classIdColumn;
	}

	public void setClassIdColumn(
			TableColumn<SignedChildTableModel, String> classIdColumn) {
		this.classIdColumn = classIdColumn;
	}

	public TableColumn<SignedChildTableModel, String> getAddingTimeColumn() {
		return addingTimeColumn;
	}

	public void setAddingTimeColumn(
			TableColumn<SignedChildTableModel, String> addingTimeColumn) {
		this.addingTimeColumn = addingTimeColumn;
	}

	public TableColumn<SignedChildTableModel, String> getLastNameColumn() {
		return lastNameColumn;
	}

	public void setLastNameColumn(
			TableColumn<SignedChildTableModel, String> lastNameColumn) {
		this.lastNameColumn = lastNameColumn;
	}
}
