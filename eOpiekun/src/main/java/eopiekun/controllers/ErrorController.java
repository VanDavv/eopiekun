package eopiekun.controllers;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ErrorController extends VBox{
	 @FXML private Button okButton;
     @FXML private TextArea errorField;
     
	public ErrorController(){
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("frames/errorFrame.fxml"));
	    fxmlLoader.setRoot(this);
	    fxmlLoader.setController(this);
	
	    try {
	        fxmlLoader.load();
	    } catch (IOException exception) {
	    	System.out.println("Exception in ErrorController.java, line 35 - fxmlLoader.load()");
	        throw new RuntimeException(exception);
	    }
	}
	
	public static void newError(String err) {
		newError(err, "Error!");
	}
    public static void newError(String err, String title) {
    	Stage stage;	
    		try
	    	{
	    		 stage = new Stage();
	    	}catch(IllegalStateException e) {
	    		System.out.println("Exception in creating a new Stage. ErrorController.java , line 45");
	    		throw new RuntimeException(e);
	    	}
		ErrorController errCon = new ErrorController();
		errCon.setError(err);
		stage.setScene(new Scene(errCon));
		stage.setTitle(title);
		stage.show();
    }
	
    public String getError() {
		return errorField.getText();
	}

	public void setError(String errMessage) {
		errorField.setText(errMessage);
	}

    @FXML
    void okButtonAction(ActionEvent event) {
    	Actions.closeAction(this);
    }

}

