package eopiekun.main;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import eopiekun.controllers.MainController;
import eopiekun.tables.Adder;
import eopiekun.tools.AppTimer;
import eopiekun.web.Manager;

public class Launcher extends Application {
	AppTimer appTimer;
	private void initialize(MainController tester) {
		new Adder(tester);
		appTimer = new AppTimer();
	}

	@Override
	public void start(Stage stage) throws Exception {
		
		MainController tester = new MainController();
		Scene scene = new Scene(tester);
		
		initialize(tester);
		appTimer.start();
		scene.getStylesheets().add(getClass().getClassLoader().getResource("style.css").toExternalForm());
		
		stage.setScene(scene);
		stage.setTitle("E-Opiekun");
		stage.centerOnScreen();
		stage.setFullScreen(true);
		stage.setFullScreenExitHint("E-Opiekun v1.0 \r\n� Copyrights by �ukasz Pi�atowski #swag");
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void stop() {
		appTimer.stop();
		Manager.close();
	}
}
